# GAME MONSTRO

> Mini Game estilo RPG, onde o objetivo seja derrotar o Monstro.

![Tela do Jogo](./public/home.png)

## 💻 Pré-requisitos

Antes de começar, verifique se você atendeu aos seguintes requisitos:

* Você instalou a versão mais recente de `Node.js`
* Baixar [API para Ranking](https://gitlab.com/j.matheussouza2019/ranking-api)
* Você leu documentação `React Js`.

## 🚀 Instalando Game Monster

Para instalar o Game Monster, siga estas etapas:

Clonando o Projeto
```
git clone https://gitlab.com/j.matheussouza2019/game.git
```

Entre na Pasta do Projeto: 
```
cd game
```

Baixar dependências
``` 
npm install 
``` 
ou caso use YARN 
``` 
yarn 
```

## ☕ Usando o Jogo

Inicie o server da API:

Inicie a API de Ranking, leia a [documentação](https://gitlab.com/j.matheussouza2019/ranking-api).

Após iniciar a API, prossiga.


Para usar o jogo, siga estas etapas:

``` 
npm run start 
``` 
ou caso use YARN 
``` 
yarn start 
```

Estes comando irão iniciar o server do jogo, coloque no seu navegador: `` localhost:3000 ``

Então você já pode jogar.

Leia as regras ao Clicar no link de Regras na tela inicial do Jogo.

[⬆ Voltar ao topo](#game-monstro)
